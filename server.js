//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//como definir una peticiòn
app.get("/",function(req,res){
res.sendFile(path.join(__dirname,'index.html'));
})

app.post("/",function(req,res){
res.send("Hemos recibido su petición post");
})

app.get("/Clientes/:idcliente",function(req,res){
  var mijson = "{'idcliente':12345}"
//res.send("Aquí tienes al clientes nùmero:" + req.params.idcliente);
res.send(mijson);
})

app.put("/",function(req,res){
res.send("Hemos recibido su petición put cambiada");
})

app.delete("/",function(req,res){
res.send("Hemos recibido su petición delete");
})

//primera forma de leer archivo
app.get("/v1/movimientos",function(req,res){
 res.sendFile(path.join(__dirname,'movimientosv1.json'));

})
//segunda forma de leer archivo
var movimientosJSON = require('./movimientosv2.json');

app.get("/v2/movimientos",function(req,res){
 res.json(movimientosJSON);

})
//pasar parametros
app.get("/v2/movimientos/:id",function(req,res){
 console.log(req.params.id);
 res.send(movimientosJSON[req.params.id-1]);

})
//con query
app.get('/v2/movimientosq',function(req,res){
  console.log(req.query);
  res.send("recibido");
})
//post
var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.post('/v2/movimientos',function(req,res){
  var nuevo = req.body;//recuperar el archivo
  nuevo.id = movimientosJSON.length + 1; //modficando ID
  movimientosJSON.push(nuevo); //voy a insertar en el archivo
  res.send("Movimientos dado de alta");
})
